<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>List of open/close times</h1>
        <ul>
            <?php

            function console_log($output, $with_script_tags = true) {
              $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) .
              ');';
              if ($with_script_tags) {
                $js_code = '<script>' . $js_code . '</script>';
              }
              echo $js_code;
            }
            #$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            #console_log($actual_link);
            #$json = file_get_contents('http://laptop-service/no');
            $json = file_get_contents('http://laptop-service:5000/newer');
            #$json = file_get_contents('http://laptop-service:5000/');

            #console_log($json);
            #$obj = json_decode($json);
            #console_log($obj);
	          #$laptops = $obj->Laptops;
            #foreach ($laptops as $l) {
            echo "<li>$json</li>";
            #}
            ?>
        </ul>
    </body>
</html>
